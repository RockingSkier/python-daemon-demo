# To kick off the script, run the following from the python directory:
#   PYTHONPATH=`pwd` python testdaemon.py start

#standard python libs
import argparse
import logging
import time

#third party libs
from daemon import runner


parser = argparse.ArgumentParser(
    description="Demo to try out starting a daemon (with flags)."
)
parser.add_argument(
    '-d --daemon',
    help="Define if this script should be run as a daemon",
    action='store_true' # True is flag present
)
args = vars(parser.parse_args())


class App():
    def __init__(self):
        self.stdin_path = '/dev/null'
        self.stdout_path = '/dev/tty'
        self.stderr_path = '/dev/tty'
        self.pidfile_path = '/var/run/testdaemon/testdaemon.pid'
        self.pidfile_timeout = 5

    def run(self):
        while True:
            #Main code goes here ...
            #Note that logger level needs to be set to logging.DEBUG before
            # this shows up in the logs
            logger.debug("Debug message")
            logger.info("Info message")
            logger.warn("Warning message")
            logger.error("Error message")
            time.sleep(10)


app = App()
logger = logging.getLogger("DaemonLog")
logger.setLevel(logging.INFO)
formatter = logging.Formatter(
    "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
)
handler = logging.FileHandler("/var/log/testdaemon/testdaemon.log")
handler.setFormatter(formatter)
logger.addHandler(handler)


def run_normally():
    print('All done, no daemon started')


def run_as_daemon():
    daemon_runner = runner.DaemonRunner(app)
    #This ensures that the logger file handle does not get closed
    # during daemonization
    daemon_runner.daemon_context.files_preserve = [handler.stream]
    daemon_runner.do_action()


if __name__ == '__main__':
    if args['d __daemon']:
        run_as_daemon()
    else:
        run_normally()
